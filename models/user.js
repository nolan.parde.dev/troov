const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const validator = require('validator');
require('dotenv').config();


const { Schema } = mongoose;
/**
 *  @class User
 *  @type {Object}
 *  @property {string} mail Adresse mail
 *  @property {string} password Mot de passe
 */

const UserSchema = new Schema({
    mail: {
        type: String,
        required: [true, 'Adresse mail obligatoire.'],
        trim: true,
        lowercase: true,
        unique: true,
        validate: [validator.isEmail, 'Adresse mail incorrecte.'],
    },
    password: {
        type: String,
        required: [true, 'Mot de passe obligatoire.'],
        minLength: [10, 'Le mot de passe doit etre au minimum de 10 caratères.'],
    },
    createdAt: {
        type: Date,
        default: Date.now(),
    },

});

/**
 * Encryption du mot de passe
 * @method
 * @param {string} event Evenement du cycle de vie
 * @param {Object} next
 */
UserSchema.pre('save', function (next) {
    const user = this;
    user.isModified()
    if (user.isModified('password') || user.isNew) {
        try {
            const salt = bcrypt.genSaltSync();
            const hash = bcrypt.hashSync(user.password, salt);
            user.password = hash;
            next();
        } catch (error) {
            throw error;
        }
    }
    next();
});


/**
 * Comparaison du mot passe enregistré et celui renseigné
 * @method
 * @param {string} event Evenement du cycle de vie
 * @param {Object} next
 * @return {boolean} 
 */
UserSchema.methods.comparePassword = function (hash) {
    const user = this;
    return bcrypt.compareSync(hash, user.password);
};

const User = mongoose.model('User', UserSchema);
module.exports = User;
