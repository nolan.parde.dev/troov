/**
 * Description
 * @param {any} 'mongoose'
 * @returns {any}
 */
const mongoose = require('mongoose');

const { Schema } = mongoose;
/**
 *  @class Idea
 *  @type {Object}
 *  @property {string} title Titre
 *  @property {string} description Description
 */
const IdeaSchema = new Schema({
    title: {
        type: String,
        required: [true, 'Titre obligatoire.'],
        minLength: [6, 'Le titre etre au minimum de 6 caratères.'],
    },
    description: {
        type: String,
        required: [true, 'Description obligatoire.'],
        minLength: [30, 'La description doit etre au minimum de 30 caratères.'],
    },
    createdAt: {
        type: Date,
        default: Date.now(),
    },
    updatedAt: {
        type: Date,
        default: Date.now(),
    },

});

/**
 * Mise à jour de l'attribut date de mise à jour
 * @method
 * @param {string} event Evenement du cycle de vie
 * @param {Object} next
 */
IdeaSchema.pre('save', function (next) {
    const idea = this;
    if (idea.isModified() || this.isNew) {
        idea.updatedAt = Date.now();
    }
    next();
});

const Idea = mongoose.model('Idea', IdeaSchema);
module.exports = Idea;
