const bodyParser = require('body-parser');
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors')
require('dotenv').config();

const { createUser, loginUser } = require('./controllers/user');
const {
    createIdea, updateIdeaById, deleteIdeaById, getIdeaById, getIdeas,
} = require('./controllers/idea');
const { verifyToken } = require('./controllers/middleware');

const app = express();
const port = 8000;

app.use(cors());
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

mongoose.connect(
    process.env.MONGO_URI,
    {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        dbName: process.env.DATABASE_NAME,
        autoIndex: true,
    },
)
    .then(() => console.log('MongoDB connection OK'))
    .catch((err) => console.log(`MongoDB connection KO ${err}`));

app.post('/user', createUser);

app.post('/login', loginUser);

app.post('/idea', verifyToken, createIdea);

app.put('/idea/:id', verifyToken, updateIdeaById);

app.delete('/idea/:id', verifyToken, deleteIdeaById);

app.get('/idea/:id', verifyToken, getIdeaById);

app.get('/idea', verifyToken, getIdeas);

app.listen(port, () => {
    console.log(`API run at port ${port}`);
});
