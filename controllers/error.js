module.exports = {
    /**
     * Permet de centraliser et  mapper en un objet d'erreur pour l'ensemble
     * de l'application
     * @method
     * @param {Object} err Object d'erreur prenant différentes formes
     * @returns {Object} object Object gloabal des erreurs
     * @returns {Object} object.duplicate Tableau de message traitant de la duplication non autorisé de données
     * @returns {Object} object.messages Tableau de message traitant des erreurs de validations de données
     * @returns {Object} object.other Tableau de message d'erreur non traité
     */
    catchErrorDatabaseToObject: (err) => {
        const result = {};
        if (err.errors) {
            const arrayError = Object.values(err.errors);
            if (arrayError.length > 0) {
                result.messages = arrayError.map((e) => {
                    if (e.properties && e.properties.message) {
                        return e.properties.message;
                    }
                });
            }
        }
        else if (err.code === 11000) {
            result.duplicate = 'L\'adresse mail est déja utilisé.';
        }
        else {
            result.other = 'Erreur serveur rencontré. Contacter administrateur';
        }
        return result;
    },
};
