const jwt = require('jsonwebtoken');
const UserModel = require('../models/user');
const { catchErrorDatabaseToObject } = require('./error');
const { sendResponse } = require('./http');

module.exports = {
    /**
     * Création d'un utilisateur
     * @method
     * @param {Object} req Objet requete
     * @param {Object} res Objet réponse
     */
    createUser: async (req, res) => {
        const { mail, password } = req.body;
        try {
            const user = new UserModel({ mail, password });
            await user.save();
            sendResponse(res, 201, {});
        } catch (error) {
            const data = catchErrorDatabaseToObject(error);
            sendResponse(res, 500, data);
        }
    },
    /**
     * Connexion de l'utilisateur
     * @method
     * @param {Object} req Objet requete
     * @param {Object} res Objet réponse
     */
    loginUser: async (req, res) => {
        const { mail, password } = req.body;
        let user = new UserModel({ mail, password });
        try {
            const err = user.validateSync();
            if (err) {
                throw err;
            }
            user = await UserModel.findOne({ mail });
            if (!user) {
                sendResponse(res, 500, { messages: ['Utilisateur inexistant.'] });
            } else {
                const isMatch = user.comparePassword(password);
                if (!isMatch) {
                    sendResponse(res, 500, { messages: ['Mot de passe incorrect.'] });
                } else {
                    const token = jwt.sign(JSON.stringify(user), process.env.SECRET_KEY);
                    sendResponse(res, 200, { token });
                }
            }
        } catch (error) {
            const data = catchErrorDatabaseToObject(error);
            sendResponse(res, 500, data);
        }
    },
};
