const jwt = require('jsonwebtoken');
const { catchErrorDatabaseToObject } = require('./error');
const { sendResponse } = require('./http');

module.exports = {
    /**
     * Permet de vérifier la présence et véracité du token
     * @method
     * @param {Object} req Objet requete
     * @param {Object} res Objet réponse
     * @param {Object} next
     */
    verifyToken: (req, res, next) => {
        const secretKey = process.env.SECRET_KEY;
        const bearerHeader = req.headers.authorization;
        try {
            if (bearerHeader) {
                const bearer = bearerHeader.split(' ');
                const token = bearer[1];
                jwt.verify(token, secretKey, (error) => {
                    if (error) {
                        sendResponse(res, 403, { messages: ['Unauthorized'] });
                    } else {
                        next();
                    }
                });
            } else {
                sendResponse(res, 403, { messages: ['Require authentification'] });
            }


        } catch (error) {
            const data = catchErrorDatabaseToObject(error);
            sendResponse(res, 500, data);
        }
    },
};
