module.exports = {
    /**
     * Envoie de la réponse http et de son contenu
     * @method
     * @param {Object} res Objet Réponse
     * @param {number} statusCode Code HTTP
     * @param {Object} data Donnée de la requete
     */
    sendResponse: (res, statusCode, data) => {
        res.status(statusCode).json(data);
    },
};
