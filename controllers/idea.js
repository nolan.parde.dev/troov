const IdeaModel = require('../models/idea');

const { ObjectId } = require('mongoose').Types;
const { catchErrorDatabaseToObject } = require('./error');
const { sendResponse } = require('./http');

module.exports = {
    /**
     * @method
     * Création d'une idée
     * @param {Object} req Objet requete
     * @param {Object} res Objet réponse
     */
    createIdea: async (req, res) => {
        const { title, description } = req.body;
        try {
            const idea = new IdeaModel({ title, description });
            await idea.save();
            sendResponse(res, 201);
        } catch (error) {
            const data = catchErrorDatabaseToObject(error);
            sendResponse(res, 500, data);
        }
    },
    /**
     * Mise à jour d'une idée par son  identifiant
     * @method
     * @param {Object} req Objet requete
     * @param {Object} res Objet réponse
     */
    updateIdeaById: async (req, res) => {
        const { title, description } = req.body;
        const { id } = req.params;
        try {
            if (!title && !description) {
                sendResponse(res, 500, { messages: ['Paramètre(s) manquant(s)'] });
            } else {
                const isId = ObjectId.isValid(id);
                if (!isId) {
                    sendResponse(res, 500, { messages: ['L\'identifiant n\'est pas valide.'] });
                } else {
                    await IdeaModel.findByIdAndUpdate(id, { title, description }, { runValidators: true });
                    sendResponse(res, 200);
                }
            }
        } catch (error) {
            const data = catchErrorDatabaseToObject(error);
            sendResponse(res, 500, data);
        }
    },
    /**
     * Suppression d'une idée par son identifiant
     * @method
     * @param {Object} req Objet requete
     * @param {Object} res Objet réponse
     */
    deleteIdeaById: async (req, res) => {
        const { id } = req.params;
        try {
            const isId = ObjectId.isValid(id);
            if (!isId) {
                sendResponse(res, 500, { messages: ['L\'identifiant n\'est pas valide.'] });
            } else {
                const isExist = await IdeaModel.exists({ _id: id });
                if (!isExist) {
                    sendResponse(res, 500, { messages: ['Ressource inexistante.'] });
                } else {
                    await IdeaModel.deleteOne({ _id: id });
                    sendResponse(res, 204, {});
                }
            }
        } catch (error) {
            const data = catchErrorDatabaseToObject(error);
            sendResponse(res, 500, data);
        }
    },
    /**
     * Obtenir une idée par son identifiant
     * @method
     * @param {Object} req Objet requete
     * @param {Object} res Objet réponse
     */
    getIdeaById: async (req, res) => {
        const { id } = req.params;
        try {
            const isId = ObjectId.isValid(id);
            if (!isId) {
                sendResponse(res, 500, { messages: ['L\'identifiant n\'est pas valide.'] });
            } else {
                const isExist = await IdeaModel.exists({ _id: id });
                if (!isExist) {
                    sendResponse(res, 500, { messages: ['Ressource inexistante.'] });
                } else {
                    const idea = await IdeaModel.findById(id);
                    if (!idea) {
                        sendResponse(res, 500, { messages: ['Ressource inexistante.'] });
                    } else {
                        sendResponse(res, 200, { idea });
                    }
                }
            }
        } catch (error) {
            const data = catchErrorDatabaseToObject(error);
            sendResponse(res, 500, data);
        }
    },
    /**
     * Obtenir l'ensemble des idées
     * @method
     * @param {Object} req Objet requete
     * @param {Object} res Objet réponse
     */
    getIdeas: async (req, res) => {
        try {
            const data = await IdeaModel.find();
            sendResponse(res, 200, { ideas: data });
        } catch (error) {
            const data = catchErrorDatabaseToObject(error);
            sendResponse(res, 500, data);
        }
    },
};
