# troov

Test technique Troov

## Installation

Lancer le docker compose à la racine du projet pour monter la base de donnée.
```
docker-compose build
docker-compose up
```
Crée un fichier .env en prenant le contenu du fichier .env.example à la racine du projet.
```
MONGO_URI="mongodb://admin:pass@localhost:60017"
DATABASE_NAME="troov"
SECRET_KEY="troov"
```
Installer les dépendances node_modules.
```
npm i
```
Lancer l'api à la racine du projet
```
node index.js
```

Lancer le front depuis le dossier views.
```
npm run dev 
```
### URL 

API : http://localhost:8000
FRONT : http://localhost:3000
MONGO-EXPRESS : http://localhost:6082